package com.phoenix4go.p4go.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class P4goConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(P4goConfigServerApplication.class, args);
	}
}
