package com.phoenix4go.p4go.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class P4goEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(P4goEurekaServerApplication.class, args);
	}
}
